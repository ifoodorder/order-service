package cz.ifoodorder.orderservice.db.dto;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.orderservice.db.pojo.Delivery;

public interface DeliveryDto extends JpaRepository<Delivery, UUID> {

}
