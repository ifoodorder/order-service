package cz.ifoodorder.orderservice.db.dto;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.orderservice.db.pojo.Address;

public interface AddressDto extends JpaRepository<Address, UUID> {

}
