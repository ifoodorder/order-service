package cz.ifoodorder.orderservice.db.dto;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.orderservice.db.pojo.OrderItem;

public interface OrderItemDto extends JpaRepository<OrderItem, UUID> {
}
