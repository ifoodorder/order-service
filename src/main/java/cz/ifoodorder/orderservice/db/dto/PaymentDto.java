package cz.ifoodorder.orderservice.db.dto;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.orderservice.db.pojo.Payment;

public interface PaymentDto extends JpaRepository<Payment, UUID> {

}
