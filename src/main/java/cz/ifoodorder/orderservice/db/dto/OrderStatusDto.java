package cz.ifoodorder.orderservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.orderservice.db.pojo.OrderStatus;

public interface OrderStatusDto extends JpaRepository<OrderStatus, UUID> {
	Optional<OrderStatus> findOneByName(String name);
}
