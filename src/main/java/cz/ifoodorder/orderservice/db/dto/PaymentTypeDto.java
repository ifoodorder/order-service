package cz.ifoodorder.orderservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.orderservice.db.pojo.PaymentType;

public interface PaymentTypeDto extends JpaRepository<PaymentType, UUID> {
	Optional<PaymentType> findOneByName(String name);
}
