package cz.ifoodorder.orderservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import cz.ifoodorder.orderservice.db.pojo.Order;

public interface OrderDto extends PagingAndSortingRepository<Order, UUID>, JpaSpecificationExecutor<Order> {
	Optional<Order> findFirstByRestaurantIdOrderByOrderNumberDesc(UUID restaurantId);
}
