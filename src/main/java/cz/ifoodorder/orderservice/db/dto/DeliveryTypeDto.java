package cz.ifoodorder.orderservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.orderservice.db.pojo.DeliveryType;

public interface DeliveryTypeDto extends JpaRepository<DeliveryType, UUID> {
	Optional<DeliveryType> findOneByName(String name);

}
