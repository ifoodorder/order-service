package cz.ifoodorder.orderservice.db.dto;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.orderservice.db.pojo.Currency;

@Deprecated
public interface CurrencyDto extends JpaRepository<Currency, UUID> {
	public Currency getOneByName(String name);
}
