package cz.ifoodorder.orderservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.orderservice.db.pojo.OrderSource;

public interface OrderSourceDto extends JpaRepository<OrderSource, UUID> {
	Optional<OrderSource> findOneByName(String name);

}
