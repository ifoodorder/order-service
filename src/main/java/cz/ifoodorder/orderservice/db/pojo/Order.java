package cz.ifoodorder.orderservice.db.pojo;

import java.time.Instant;
import java.util.Collection;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "order_table")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private UUID restaurantId;
	private Long orderNumber;
	private Instant timestamp;
	private String note;
	private String contactPhone;
	@ManyToOne
	private OrderStatus orderStatus;
	@ManyToOne
	private Address address;
	@ManyToOne
	private OrderSource orderSource;
	@ManyToOne(cascade = CascadeType.ALL)
	private Payment payment;
	@ManyToOne(cascade = CascadeType.ALL)
	private Delivery delivery;
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
	private Collection<OrderItem> orderItems;
	private UUID customer;
}
