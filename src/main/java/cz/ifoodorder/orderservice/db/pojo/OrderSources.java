package cz.ifoodorder.orderservice.db.pojo;

public interface OrderSources {
	public static final String E_SHOP = "E-SHOP";
	public static final String RESTAURANT = "RESTAURANT";
}
