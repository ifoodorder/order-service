package cz.ifoodorder.orderservice.restaurants;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViewFeeType {
	private UUID id;
	private String name;
}
