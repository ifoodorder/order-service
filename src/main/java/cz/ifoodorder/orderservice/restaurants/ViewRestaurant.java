package cz.ifoodorder.orderservice.restaurants;

import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViewRestaurant {
	private UUID id;
	private UUID userId;
	private List<ViewDeliverySettings> deliverySettings;
}
