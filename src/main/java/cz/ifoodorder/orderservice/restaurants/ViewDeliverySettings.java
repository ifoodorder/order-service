package cz.ifoodorder.orderservice.restaurants;

import java.math.BigDecimal;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViewDeliverySettings {
	private UUID id;
	private BigDecimal minOrderAmount;
	private Long deliveryTimeOffset;
	private Long deliveryLimit;
	private boolean enabled;
	private ViewFeeType feeType;
	private BigDecimal fee;

}
