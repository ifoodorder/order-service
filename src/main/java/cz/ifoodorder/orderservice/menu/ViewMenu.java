package cz.ifoodorder.orderservice.menu;

import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ViewMenu {
	private UUID id;
	private UUID restaurantId;
	private List<ViewCategory> categories;
}
