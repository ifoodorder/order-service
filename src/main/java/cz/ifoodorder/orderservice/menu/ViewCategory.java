package cz.ifoodorder.orderservice.menu;

import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViewCategory {
	private UUID id;
	private List<ViewItem> items;
}
