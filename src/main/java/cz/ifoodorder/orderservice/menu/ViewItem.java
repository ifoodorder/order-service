package cz.ifoodorder.orderservice.menu;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViewItem {
	private UUID id;
	private String name;
}
