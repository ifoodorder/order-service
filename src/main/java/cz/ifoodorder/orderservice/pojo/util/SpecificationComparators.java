package cz.ifoodorder.orderservice.pojo.util;

import cz.ifoodorder.orderservice.pojo.util.comparator.*;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum SpecificationComparators {
	EQ(new EqualComparator()),
	GT(new GreaterThanComparator()),
	GTEQ(new GreaterOrEqualToComparator()),
	LT(new LessThanComparator()),
	LTEQ(new LessOrEqualToComparator()),
	NE(new NotEqualComparator());

	public static SpecificationComparator findByString(String comparator) {
		try {			
			return SpecificationComparators.valueOf(comparator.toUpperCase()).comparator;
		} catch (Exception ex) {
			log.error("Failed to find comparator for: {}", comparator);
			return EQ.comparator;
		}
	}
	
	private final SpecificationComparator comparator;
	SpecificationComparators(SpecificationComparator comparator) {
		this.comparator = comparator;
	}
}
