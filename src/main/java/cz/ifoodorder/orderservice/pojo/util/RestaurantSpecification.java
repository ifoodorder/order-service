package cz.ifoodorder.orderservice.pojo.util;

import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import cz.ifoodorder.orderservice.db.pojo.Order;

public class RestaurantSpecification implements Specification<Order> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8956601166090414604L;
	
	private final UUID restaurantId;
	
	public RestaurantSpecification(UUID restaurantId) {
		this.restaurantId = restaurantId;
	}

	@Override
	public Predicate toPredicate(Root<Order> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		return criteriaBuilder.equal(root.get("restaurantId"), restaurantId);
	}
}
