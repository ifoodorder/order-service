package cz.ifoodorder.orderservice.pojo.util;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

public interface SpecificationComparator {
	// Numbers
    Predicate compareNumber(Expression<Number> x, Number y, CriteriaBuilder builder);
    Predicate compareNumber(Expression<Number> x, Expression<Number> y, CriteriaBuilder builder);
    
    // Comparables
	<T extends Comparable<? super T>> Predicate compare(CriteriaBuilder builder, Expression<T> x, T y);
	<T extends Comparable<? super T>> Predicate compare(CriteriaBuilder builder, Expression<T> x, Expression<T> y);
}
