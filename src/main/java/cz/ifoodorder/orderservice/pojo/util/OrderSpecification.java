package cz.ifoodorder.orderservice.pojo.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;

import cz.ifoodorder.orderservice.api.pojo.OrderFilter;
import cz.ifoodorder.orderservice.db.pojo.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OrderSpecification implements Specification<Order> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4294130805129139701L;
	private final OrderFilter orderFilter;
	public OrderSpecification(OrderFilter orderFilter) {
		this.orderFilter = orderFilter;
	}

	@Override
	public Predicate toPredicate(Root<Order> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<>();
		if(orderFilter.isContactPhone()) {
			predicates.add(criteriaBuilder.like(root.get("contactPhone"),"%" + orderFilter.getContactPhone() + "%"));
		}
		if(orderFilter.isPrice()) {
			log.warn("Price filter missing implementation!");
		}
		if(orderFilter.isCustomerName()) {
			log.warn("CustomerName filter missing implementation!");
		}
		if(orderFilter.isRestaurantName()) {
			log.warn("RestaurantName filter missing implementation!");
		}
		if(orderFilter.isOrderId()) {
			var comparator = SpecificationComparators.findByString(orderFilter.getOrderIdComparator());
			var predicate = comparator.compareNumber(root.get("orderNumber"), orderFilter.getOrderId(), criteriaBuilder);
			predicates.add(predicate);
		}
		if(orderFilter.isOrderStatus()) {
			log.debug("OrderStatus: {}", orderFilter.getOrderStatus());
			predicates.add(criteriaBuilder.equal(root.get("orderStatus").get("name"), orderFilter.getOrderStatus()));
		}
		if(orderFilter.isPaymentStatus()) {
			log.warn("PaymentStatus filter missing implementation!");
		}
		if(orderFilter.isDeliveryAddress()) {
			log.debug("DeliveryAddress: {}", orderFilter.getDeliveryAddress());
			var address = root.get("address");
			Expression<String> street = criteriaBuilder.concat(address.get("street"), " ");
			Expression<String> building = criteriaBuilder.concat(address.get("building"), ", ");
			Expression<String> zip = criteriaBuilder.concat(address.get("zipCode"), ", ");
			Expression<String> city = address.get("city");
			Expression<String> streetBuilding = criteriaBuilder.concat(street, building);
			Expression<String> zipCity = criteriaBuilder.concat(zip, city);
			Expression<String> fullAddress = criteriaBuilder.concat(streetBuilding, zipCity);
			predicates.add(criteriaBuilder.like(fullAddress, "%" + orderFilter.getDeliveryAddress() + "%"));
		}
		if(orderFilter.isDeliveryDate()) {
			log.debug("DeliveryDate: {} {}", orderFilter.getDeliveryDateComparator(), orderFilter.getDeliveryDate());
			var comparator = SpecificationComparators.findByString(orderFilter.getDeliveryDateComparator());
			var predicate = comparator.compare(criteriaBuilder, root.get("delivery").get("deliveryDate"), orderFilter.getDeliveryDate());
			predicates.add(predicate);
		}
		if(orderFilter.isDeliveryTime()) {
			log.warn("DeliveryTime filter missing implementation!");
		}
		if(orderFilter.isDeliveryType()) {
			log.debug("DeliveryType: {}", orderFilter.getDeliveryType());
			predicates.add(criteriaBuilder.equal(root.get("delivery").get("deliveryType").get("name"), orderFilter.getDeliveryType()));
		}
		
		return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
	}
	
	public PageRequest getPageable() {
		int page = 0;
		int size = 10;
		Sort sort = Sort.by(Direction.DESC, "orderNumber");
		
		if(orderFilter.isOffest()) {
			size = orderFilter.getLength();
		}
		if(orderFilter.isLength()) {
			page = orderFilter.getOffset();
		}
		
		if(orderFilter.isOrderColumn()) {
			if(orderFilter.isOrderType() && orderFilter.getOrderType().equals("asc")) {
				sort = Sort.by(Direction.ASC, orderFilter.getOrderColumn());
			} else if (orderFilter.isOrderType() && orderFilter.getOrderType().equals("desc")) {
				sort = Sort.by(Direction.DESC, orderFilter.getOrderColumn());				
			}
		}
		return PageRequest.of(page, size, sort);
	}

}
