package cz.ifoodorder.orderservice.pojo.util.comparator;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

import cz.ifoodorder.orderservice.pojo.util.SpecificationComparator;

public class LessOrEqualToComparator implements SpecificationComparator {

	public LessOrEqualToComparator() {
		//
	}

	@Override
	public <T extends Comparable<? super T>> Predicate compare(CriteriaBuilder builder, Expression<T> x, T y) {
		return builder.lessThanOrEqualTo(x, y);
	}
	
	@Override
	public <T extends Comparable<? super T>> Predicate compare(CriteriaBuilder builder, Expression<T> x, Expression<T> y) {
		return builder.lessThanOrEqualTo(x, y);
	}

	@Override
	public Predicate compareNumber(Expression<Number> x, Number y, CriteriaBuilder builder) {
		return builder.le(x, y);
	}

	@Override
	public Predicate compareNumber(Expression<Number> x, Expression<Number> y, CriteriaBuilder builder) {
		return builder.le(x, y);
	}

}
