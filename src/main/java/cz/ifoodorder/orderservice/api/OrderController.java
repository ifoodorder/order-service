package cz.ifoodorder.orderservice.api;

import java.util.Collection;
import java.util.UUID;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.ifoodorder.orderservice.api.pojo.ListView;
import cz.ifoodorder.orderservice.api.pojo.OrderFilter;
import cz.ifoodorder.orderservice.api.pojo.ApiOrder;
import cz.ifoodorder.orderservice.api.pojo.mappers.OrderMapper;
import cz.ifoodorder.orderservice.api.validators.OrderValidator;
import cz.ifoodorder.orderservice.db.dto.OrderDto;
import cz.ifoodorder.orderservice.db.pojo.Order;
import cz.ifoodorder.orderservice.pojo.util.OrderSpecification;
import cz.ifoodorder.orderservice.pojo.util.RestaurantSpecification;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api")
@Slf4j
public class OrderController {
	private final OrderDto orderDto;
	private final OrderMapper orderMapper;
	private final OrderValidator orderValidator;
	public OrderController(
			@Autowired OrderDto orderDto,
			@Autowired OrderMapper orderMapper,
			@Autowired OrderValidator orderValidator) {
		this.orderDto = orderDto;
		this.orderMapper = orderMapper;
		this.orderValidator = orderValidator;
	}
	
	@PostMapping("/orders")
	public ApiOrder postOrder(@RequestBody ApiOrder viewOrder, Authentication authentication) {
		log.info("Posting order: {}", viewOrder);
		Order order = orderValidator.validatePost(orderMapper.mapToDb(viewOrder), authentication);
		
		
		
		orderDto.save(order);
		var returningOrder = orderMapper.mapToApi(order);
		log.info("{}",returningOrder);
		return returningOrder;
		
	}
	
	@GetMapping("/orders")
	public ListView<ApiOrder> getOrders(@PathParam("restaurantId") String restaurantId, OrderFilter orderFilter, Authentication authentication) {
		log.info("restaurantId: {}", restaurantId);
		var orderSpec = new OrderSpecification(orderFilter);
		var restaurantSpec = new RestaurantSpecification(UUID.fromString(restaurantId));
		var finalSpec = orderSpec.and(restaurantSpec);
		var orders = orderDto.findAll(finalSpec, orderSpec.getPageable()).getContent();
		var count = orderDto.count(finalSpec);
		Collection<ApiOrder> viewOrders = orderMapper.mapToApi(orders);
		return new ListView<>(viewOrders, count);
	}
	
	@GetMapping("/orders/{orderId}")
	public ApiOrder getOrder(@PathVariable("orderId") UUID orderId) {
		return orderMapper.mapToApi(orderDto.findById(orderId).orElseThrow());
	}
}
