package cz.ifoodorder.orderservice.api;

import java.util.UUID;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import cz.ifoodorder.orderservice.config.IFoodOrderProperties;
import cz.ifoodorder.orderservice.restaurants.ViewRestaurant;

@Component
public class RestaurantService {
	private RestTemplate restTemplate;
	private IFoodOrderProperties iFoodOrderProperties;
	
	public RestaurantService(RestTemplate restTemplate, IFoodOrderProperties iFoodOrderProperties) {
		this.restTemplate = restTemplate;
		this.iFoodOrderProperties = iFoodOrderProperties;
	}
	
	public ViewRestaurant getRestaurantById(UUID restarantId) {
		return restTemplate.getForObject(iFoodOrderProperties.getRestaurantUrl() + restarantId, ViewRestaurant.class);
	}
}
