package cz.ifoodorder.orderservice.api.pojo;

import java.io.Serializable;
import java.time.LocalDate;

import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderFilter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1468411593935272852L;
	private String contactPhone;
	private String customerName;
	private String deliveryAddress;
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate deliveryDate;
	private String deliveryDateComparator;
	private String deliveryTime;
	private String deliveryTimeComparator;
	private String deliveryType;
	private Number orderId;
	private String orderIdComparator;
	private String orderStatus;
	private String paymentStatus;
	private String price;
	private String restaurantName;
	private String orderColumn;
	private String orderType;
	private Integer length;
	private Integer offset;
	
	public boolean isLength() {
		return length != null;
	}
	public boolean isOffest() {
		return offset != null;
	}
	public boolean isOrderColumn() {
		return StringUtils.isNotEmpty(getOrderColumn());
	}
	
	public boolean isOrderType() {
		return StringUtils.isNotEmpty(getOrderType());
	}
	
	public boolean isContactPhone() {
		return StringUtils.isNotEmpty(getContactPhone());
	}
	
	public boolean isPrice() {
		return StringUtils.isNotEmpty(getPrice());
	}
	
	public boolean isCustomerName() {
		return StringUtils.isNotEmpty(getCustomerName());
	}
	
	public boolean isRestaurantName() {
		return StringUtils.isNotEmpty(getRestaurantName());
	}
	
	public boolean isOrderId() {
		return this.getOrderId() != null;
	}
	
	public boolean isOrderStatus() {
		return StringUtils.isNotEmpty(getOrderStatus());
	}
	
	public boolean isPaymentStatus() {
		return StringUtils.isNotEmpty(getPaymentStatus());
	}
	
	public boolean isDeliveryAddress() {
		return StringUtils.isNotEmpty(getDeliveryAddress());
	}
	
	public boolean isDeliveryDate() {
		return getDeliveryDate() != null;
	}
	
	public boolean isDeliveryTime() {
		return StringUtils.isNotEmpty(getDeliveryTime());
	}
	
	public boolean isDeliveryType() {
		return StringUtils.isNotBlank(getDeliveryType());
	}
}
