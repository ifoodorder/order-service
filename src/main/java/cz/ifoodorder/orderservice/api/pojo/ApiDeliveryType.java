package cz.ifoodorder.orderservice.api.pojo;

import com.fasterxml.jackson.annotation.JsonValue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiDeliveryType {
	@JsonValue
	private String name;
}
