package cz.ifoodorder.orderservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.orderservice.api.pojo.ApiPayment;
import cz.ifoodorder.orderservice.db.pojo.Payment;

@Component
public class PaymentMapper {
	private final PaymentTypeMapper paymentTypeMapper;
	
	public PaymentMapper(PaymentTypeMapper paymentTypeMapper) {
		this.paymentTypeMapper = paymentTypeMapper;
	}

	public Payment mapToDb(ApiPayment apiPayment) {
		if(apiPayment == null) {
			return null;
		}
		var payment = new Payment();
		payment.setId(apiPayment.getId());
		payment.setPaymentType(paymentTypeMapper.mapToDb(apiPayment.getPaymentType()));
		return payment;
	}
	
	public ApiPayment mapToApi(Payment payment) {
		if(payment == null) {
			return null;
		}
		var apiPayment = new ApiPayment();
		apiPayment.setId(payment.getId());
		apiPayment.setPaymentType(paymentTypeMapper.mapToApi(payment.getPaymentType()));
		return apiPayment;
	}

}
