package cz.ifoodorder.orderservice.api.pojo;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiDelivery {
	private UUID id;
	private LocalTime deliveryFrom;
	private LocalTime deliveryTo;
	private LocalDate deliveryDate;
	private ApiDeliveryType deliveryType;
	private String fee;
}
