package cz.ifoodorder.orderservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.orderservice.api.pojo.ApiOrder;
import cz.ifoodorder.orderservice.db.pojo.Order;

@Component
public class OrderMapper {
	private final AddressMapper addressMapper;
	private final DeliveryMapper deliveryMapper;
	private final OrderItemMapper orderItemMapper;
	private final OrderSourceMapper orderSourceMapper;
	private final OrderStatusMapper orderStatusMapper;
	private final PaymentMapper paymentMapper;
	
	public OrderMapper(AddressMapper addressMapper, DeliveryMapper deliveryMapper, OrderItemMapper orderItemMapper,
			OrderSourceMapper orderSourceMapper, OrderStatusMapper orderStatusMapper, PaymentMapper paymentMapper) {
		this.addressMapper = addressMapper;
		this.deliveryMapper = deliveryMapper;
		this.orderItemMapper = orderItemMapper;
		this.orderSourceMapper = orderSourceMapper;
		this.orderStatusMapper = orderStatusMapper;
		this.paymentMapper = paymentMapper;
	}
	public Order mapToDb(ApiOrder apiOrder) {
		if(apiOrder == null) {
			return null;
		}
		var order = new Order();
		order.setAddress(addressMapper.mapToDb(apiOrder.getAddress()));
		order.setContactPhone(apiOrder.getContactPhone());
		order.setCustomer(apiOrder.getCustomer());
		order.setDelivery(deliveryMapper.mapToDb(apiOrder.getDelivery()));
		order.setId(apiOrder.getId());
		order.setNote(apiOrder.getNote());
		order.setOrderItems(orderItemMapper.mapToDb(apiOrder.getOrderItems()));
		order.setOrderNumber(apiOrder.getOrderNumber());
		order.setOrderSource(orderSourceMapper.mapToDb(apiOrder.getOrderSource()));
		order.setOrderStatus(orderStatusMapper.mapToDb(apiOrder.getOrderStatus()));
		order.setPayment(paymentMapper.mapToDb(apiOrder.getPayment()));
		order.setRestaurantId(apiOrder.getRestaurantId());
		order.setTimestamp(apiOrder.getTimestamp());
		return order;
	}
	
	public ApiOrder mapToApi(Order order) {
		if(order == null) {
			return null;
		}
		var apiOrder = new ApiOrder();
		apiOrder.setAddress(addressMapper.mapToApi(order.getAddress()));
		apiOrder.setContactPhone(order.getContactPhone());
		apiOrder.setCustomer(order.getCustomer());
		apiOrder.setDelivery(deliveryMapper.mapToApi(order.getDelivery()));
		apiOrder.setId(order.getId());
		apiOrder.setNote(order.getNote());
		apiOrder.setOrderItems(orderItemMapper.mapToApi(order.getOrderItems()));
		apiOrder.setOrderNumber(order.getOrderNumber());
		apiOrder.setOrderSource(orderSourceMapper.mapToApi(order.getOrderSource()));
		apiOrder.setOrderStatus(orderStatusMapper.mapToApi(order.getOrderStatus()));
		apiOrder.setPayment(paymentMapper.mapToApi(order.getPayment()));
		apiOrder.setRestaurantId(order.getRestaurantId());
		apiOrder.setTimestamp(order.getTimestamp());
		return apiOrder;
	}
	
	public Collection<ApiOrder> mapToApi(Collection<Order> orders) {
		return orders.stream().map(this::mapToApi).collect(Collectors.toList());
	}
	
	public Collection<Order> mapToDb(Collection<ApiOrder> apiOrders) {
		return apiOrders.stream().map(this::mapToDb).collect(Collectors.toList());
	}
}
