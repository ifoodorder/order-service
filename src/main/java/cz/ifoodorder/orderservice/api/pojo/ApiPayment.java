package cz.ifoodorder.orderservice.api.pojo;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiPayment {
	private UUID id;
	private ViewPaymentType paymentType;
}
