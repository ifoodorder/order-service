package cz.ifoodorder.orderservice.api.pojo.mappers;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.orderservice.api.pojo.ApiOrderItem;
import cz.ifoodorder.orderservice.db.pojo.OrderItem;

@Component
public class OrderItemMapper {
	
	public OrderItem mapToDb(ApiOrderItem viewOrderItem) {
		if(viewOrderItem == null) {
			return null;
		}
		var orderItem = new OrderItem();
		orderItem.setCount(viewOrderItem.getCount());
		orderItem.setId(viewOrderItem.getId());
		orderItem.setItemId(viewOrderItem.getItemId());
		orderItem.setNote(viewOrderItem.getNote());
		orderItem.setUnitPrice(new BigDecimal(viewOrderItem.getUnitPrice()));
		return orderItem;
	}
	
	public ApiOrderItem mapToApi(OrderItem orderItem) {
		if (orderItem == null) {
			return null;
		}
		var apiOrderItem = new ApiOrderItem();
		apiOrderItem.setCount(orderItem.getCount());
		apiOrderItem.setId(orderItem.getId());
		apiOrderItem.setItemId(orderItem.getItemId());
		apiOrderItem.setNote(orderItem.getNote());
		apiOrderItem.setUnitPrice(orderItem.getUnitPrice().toPlainString());
		return apiOrderItem;
	}
	
	public Collection<OrderItem> mapToDb(Collection<ApiOrderItem> apiOrderItems) {
		return apiOrderItems.stream().map(this::mapToDb).collect(Collectors.toList());
	}
	
	public Collection<ApiOrderItem> mapToApi(Collection<OrderItem> orderItems) {
		return orderItems.stream().map(this::mapToApi).collect(Collectors.toList());
	}
}
