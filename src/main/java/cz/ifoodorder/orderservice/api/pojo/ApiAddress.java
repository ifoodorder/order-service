package cz.ifoodorder.orderservice.api.pojo;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiAddress {
	private UUID id;
	private ApiCountry country;
	private String city;
	private String zipCode;
	private String street;
	private String building;
}
