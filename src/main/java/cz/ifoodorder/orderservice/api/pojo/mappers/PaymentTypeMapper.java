package cz.ifoodorder.orderservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.orderservice.api.pojo.ViewPaymentType;
import cz.ifoodorder.orderservice.db.dto.PaymentTypeDto;
import cz.ifoodorder.orderservice.db.pojo.PaymentType;

@Component
public class PaymentTypeMapper {
	private final PaymentTypeDto dto;
	
	public PaymentTypeMapper(PaymentTypeDto dto) {
		this.dto = dto;
	}
	
	public PaymentType mapToDb(ViewPaymentType viewPaymentType) {
		if(viewPaymentType == null) {
			return null;
		}
		return dto.findOneByName(viewPaymentType.getName()).orElseThrow();
	}
	
	ViewPaymentType mapToApi(PaymentType paymentType) {
		if(paymentType == null) {
			return null;
		}
		return new ViewPaymentType(paymentType.getName());
	}

}
