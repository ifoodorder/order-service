package cz.ifoodorder.orderservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.orderservice.api.pojo.ApiDeliveryType;
import cz.ifoodorder.orderservice.db.dto.DeliveryTypeDto;
import cz.ifoodorder.orderservice.db.pojo.DeliveryType;

@Component
public class DeliveryTypeMapper {
	private final DeliveryTypeDto dto;
	
	public DeliveryTypeMapper(DeliveryTypeDto dto) {
		this.dto = dto;
	}

	public DeliveryType mapToDb(ApiDeliveryType apiDeliveryType) {
		if(apiDeliveryType == null) {
			return null;
		}
		return dto.findOneByName(apiDeliveryType.getName()).orElseThrow();
	}
	
	public ApiDeliveryType mapToApi(DeliveryType deliveryType) {
		if(deliveryType == null) {
			return null;
		}
		return new ApiDeliveryType(deliveryType.getName());
	}

}
