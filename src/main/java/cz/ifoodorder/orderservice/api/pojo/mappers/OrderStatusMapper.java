package cz.ifoodorder.orderservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.orderservice.api.pojo.ApiOrderStatus;
import cz.ifoodorder.orderservice.db.dto.OrderStatusDto;
import cz.ifoodorder.orderservice.db.pojo.OrderStatus;

@Component
public class OrderStatusMapper {
	private final OrderStatusDto dto;
	
	public OrderStatusMapper(OrderStatusDto dto) {
		this.dto = dto;
	}

	public OrderStatus mapToDb(ApiOrderStatus apiOrderSource) {
		if(apiOrderSource == null) {
			return null;
		}
		return dto.findOneByName(apiOrderSource.getName()).orElseThrow();
	}
	
	public ApiOrderStatus mapToApi(OrderStatus orderSource) {
		if(orderSource == null) {
			return null;
		}
		return new ApiOrderStatus(orderSource.getName());
	}

}
