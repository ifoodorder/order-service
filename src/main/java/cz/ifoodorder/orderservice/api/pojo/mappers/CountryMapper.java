package cz.ifoodorder.orderservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.orderservice.api.pojo.ApiCountry;
import cz.ifoodorder.orderservice.db.dto.CountryDto;
import cz.ifoodorder.orderservice.db.pojo.Country;

@Component
public class CountryMapper {
	private final CountryDto dto;
	
	public CountryMapper(CountryDto dto) {
		this.dto = dto;
	}
	
	public Country mapToDb(ApiCountry apiCountry) {
		if(apiCountry == null) {
			return null;
		}
		return dto.findOneByName(apiCountry.getName()).orElseThrow();
	}
	
	public ApiCountry mapToApi(Country country) {
		if(country == null) {
			return null;
		}
		return new ApiCountry(country.getName());
	}

}
