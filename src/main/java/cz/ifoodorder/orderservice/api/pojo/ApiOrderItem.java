package cz.ifoodorder.orderservice.api.pojo;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiOrderItem {
	private UUID id;
	private UUID itemId;
	private Integer count;
	private String note;
	private String unitPrice;
}
