package cz.ifoodorder.orderservice.api.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequest {
	private OrderDetails details;
	private OrderItems items;
	private OrderRestaurant restaurant;
}
