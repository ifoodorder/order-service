package cz.ifoodorder.orderservice.api.pojo;

import java.util.Collection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListView <T> {
	public ListView (Collection<T> data) {
		this(data, data.size());
	}
	public ListView (Collection<T> data, long count) {
		this.data = data;
		this.count = count;
	}
	private long count;
	private Collection<T> data;
}
