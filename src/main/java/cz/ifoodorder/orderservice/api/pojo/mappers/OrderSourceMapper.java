package cz.ifoodorder.orderservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.orderservice.api.pojo.ApiOrderSource;
import cz.ifoodorder.orderservice.db.dto.OrderSourceDto;
import cz.ifoodorder.orderservice.db.pojo.OrderSource;

@Component
public class OrderSourceMapper {
	private final OrderSourceDto dto;
	
	public OrderSourceMapper(OrderSourceDto dto) {
		this.dto = dto;
	}
	
	public OrderSource mapToDb(ApiOrderSource apiOrderSource) {
		if(apiOrderSource == null) {
			return null;
		}
		return dto.findOneByName(apiOrderSource.getName()).orElseThrow();
	}
	
	public ApiOrderSource mapToApi(OrderSource orderSource) {
		if(orderSource == null) {
			return null;
		}
		return new ApiOrderSource(orderSource.getName());
	}

}
