package cz.ifoodorder.orderservice.api.pojo;

import com.fasterxml.jackson.annotation.JsonValue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiOrderStatus {
	@JsonValue
	private String name;
}
