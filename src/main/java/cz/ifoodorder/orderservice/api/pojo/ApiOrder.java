package cz.ifoodorder.orderservice.api.pojo;

import java.time.Instant;
import java.util.Collection;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiOrder {
	private UUID id;
	private UUID restaurantId;
	private Long orderNumber;
	private Instant timestamp;
	private String note;
	private String contactPhone;
	private ApiOrderStatus orderStatus;
	private ApiAddress address;
	private ApiOrderSource orderSource;
	private ApiPayment payment;
	private ApiDelivery delivery;
	private UUID customer;
	private Collection<ApiOrderItem> orderItems;
}
