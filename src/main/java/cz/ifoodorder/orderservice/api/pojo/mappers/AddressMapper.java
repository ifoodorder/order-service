package cz.ifoodorder.orderservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.orderservice.api.pojo.ApiAddress;
import cz.ifoodorder.orderservice.db.pojo.Address;

@Component
public class AddressMapper {
	private final CountryMapper countryMapper;

	public AddressMapper(CountryMapper countryMapper) {
		this.countryMapper = countryMapper;
	}
	
	public Address mapToDb(ApiAddress apiAddress) {
		if(apiAddress == null) {
			return null;
		}
		var address = new Address();
		address.setBuilding(apiAddress.getBuilding());
		address.setCity(apiAddress.getCity());
		address.setCountry(countryMapper.mapToDb(apiAddress.getCountry()));
		address.setId(apiAddress.getId());
		address.setZipCode(apiAddress.getZipCode());
		return address;
	}
	public ApiAddress mapToApi(Address address) {
		if(address == null) {
			return null;
		}
		var apiAddress = new ApiAddress();
		apiAddress.setBuilding(address.getBuilding());
		apiAddress.setCity(address.getCity());
		apiAddress.setCountry(countryMapper.mapToApi(address.getCountry()));
		apiAddress.setId(address.getId());
		apiAddress.setZipCode(address.getZipCode());
		return apiAddress;
	}
}
