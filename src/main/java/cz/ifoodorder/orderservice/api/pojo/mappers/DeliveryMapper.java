package cz.ifoodorder.orderservice.api.pojo.mappers;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import cz.ifoodorder.orderservice.api.pojo.ApiDelivery;
import cz.ifoodorder.orderservice.db.pojo.Delivery;

@Component
public class DeliveryMapper {
	private final DeliveryTypeMapper deliveryTypeMapper;
	
	public DeliveryMapper(DeliveryTypeMapper deliveryTypeMapper) {
		this.deliveryTypeMapper = deliveryTypeMapper;
	}
	
	public Delivery mapToDb(ApiDelivery apiDelivery) {
		if(apiDelivery == null) {
			return null;
		}
		var delivery = new Delivery();
		delivery.setDeliveryDate(apiDelivery.getDeliveryDate());
		delivery.setDeliveryFrom(apiDelivery.getDeliveryFrom());
		delivery.setDeliveryTo(apiDelivery.getDeliveryTo());
		delivery.setDeliveryType(deliveryTypeMapper.mapToDb(apiDelivery.getDeliveryType()));
		delivery.setId(apiDelivery.getId());
		delivery.setFee(new BigDecimal(apiDelivery.getFee()));
		return delivery;
	}
	
	public ApiDelivery mapToApi(Delivery delivery) {
		if(delivery == null) {
			return null;
		}
		var apiDelivery = new ApiDelivery();
		apiDelivery.setDeliveryDate(delivery.getDeliveryDate());
		apiDelivery.setDeliveryFrom(delivery.getDeliveryFrom());
		apiDelivery.setDeliveryTo(delivery.getDeliveryTo());
		apiDelivery.setDeliveryType(deliveryTypeMapper.mapToApi(delivery.getDeliveryType()));
		apiDelivery.setId(delivery.getId());
		apiDelivery.setFee(delivery.getFee().toPlainString());
		return apiDelivery;
	}
}
