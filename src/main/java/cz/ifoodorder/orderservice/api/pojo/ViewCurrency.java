package cz.ifoodorder.orderservice.api.pojo;

import cz.ifoodorder.orderservice.db.pojo.Currency;

@Deprecated
public enum ViewCurrency {
	CZK,
	EUR,
	USD;
	
	public static ViewCurrency convertPojoToView(Currency currency) {
		return ViewCurrency.valueOf(currency.getName());
	}
}
