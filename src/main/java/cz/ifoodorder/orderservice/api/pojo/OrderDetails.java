package cz.ifoodorder.orderservice.api.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetails {
	private String name;
	private String phone;
	private String email;
	private String deliveryOption;
	private String courierCity;
	private String courierStreet;
	private String courierZip;
	private String courierBuilding;
	private String pickupPoint;
	private String deliveryTimeOption;
	private String deliveryTime;
	private String deliveryDate;
	private String paymentType;
}
