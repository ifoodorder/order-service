package cz.ifoodorder.orderservice.api;

import java.util.UUID;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import cz.ifoodorder.orderservice.config.IFoodOrderProperties;
import cz.ifoodorder.orderservice.menu.ViewMenu;

@Component
public class MenuService {
	private final IFoodOrderProperties iFoodOrderProperties;
	private final RestTemplate restTemplate;
	public MenuService(IFoodOrderProperties iFoodOrderProperties, RestTemplate restTemplate) {
		this.iFoodOrderProperties = iFoodOrderProperties;
		this.restTemplate = restTemplate;
	}
	
	public ViewMenu getMenuByRestaurant(UUID restaurant) {
		return restTemplate.getForObject(iFoodOrderProperties.getMenuUrl() + restaurant, ViewMenu.class);
	}
}
