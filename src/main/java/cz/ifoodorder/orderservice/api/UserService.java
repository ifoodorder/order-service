package cz.ifoodorder.orderservice.api;

import java.util.UUID;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import cz.ifoodorder.orderservice.config.IFoodOrderProperties;
import cz.ifoodorder.orderservice.db.pojo.Order;
import cz.ifoodorder.orderservice.restaurants.ViewRestaurant;

@Component
public class UserService {
	private final RestTemplate restTemplate;
	private final IFoodOrderProperties iFoodOrderProperties;
	public UserService(RestTemplate restTemplate, IFoodOrderProperties iFoodOrderProperties) {
		this.restTemplate = restTemplate;
		this.iFoodOrderProperties = iFoodOrderProperties;
	}
	
	public boolean isRestaurantOwner(Authentication authentication, Order order, ViewRestaurant restaurant) {
		if (authentication == null) {
			return false;
		}
		if (hasRole(iFoodOrderProperties.getRestaurantAdminRole(), authentication)) {
			var authUser = UUID.fromString(authentication.getName());
			return authUser.equals(restaurant.getUserId());
		}
		return false;
	}
	
	public static boolean hasRole(String role, Authentication authentication) {
		if (authentication == null) {
			return false;
		}
		if (authentication.getAuthorities().stream().anyMatch(x -> x.getAuthority().equals("ROLE_" + role))) {
			return true;
		}
		if (authentication.getAuthorities().stream().anyMatch(x -> x.getAuthority().equals(role))) {
			return true;
		}
		return false;
	}
}
