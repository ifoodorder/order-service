package cz.ifoodorder.orderservice.api.validators;

import java.time.Instant;
import java.util.Collection;
import java.util.Objects;
import java.util.UUID;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import cz.ifoodorder.orderservice.api.MenuService;
import cz.ifoodorder.orderservice.api.RestaurantService;
import cz.ifoodorder.orderservice.api.UserService;
import cz.ifoodorder.orderservice.db.dto.OrderDto;
import cz.ifoodorder.orderservice.db.dto.OrderSourceDto;
import cz.ifoodorder.orderservice.db.dto.OrderStatusDto;
import cz.ifoodorder.orderservice.db.pojo.Delivery;
import cz.ifoodorder.orderservice.db.pojo.Order;
import cz.ifoodorder.orderservice.db.pojo.OrderItem;
import cz.ifoodorder.orderservice.db.pojo.OrderSource;
import cz.ifoodorder.orderservice.db.pojo.OrderSources;
import cz.ifoodorder.orderservice.db.pojo.OrderStatus;
import cz.ifoodorder.orderservice.db.pojo.OrderStatuses;
import cz.ifoodorder.orderservice.restaurants.ViewRestaurant;

@Component
public class OrderValidator {
	private final UserService userService;
	private final MenuService menuService;
	private final RestaurantService restaurantService;
	private final OrderDto orderDto;
	private final OrderSourceDto orderSourceDto;
	private final OrderStatusDto orderStatusDto;
	public OrderValidator(
			UserService userService,
			MenuService menuService,
			RestaurantService restaurantService,
			OrderDto orderDto,
			OrderSourceDto orderSourceDto,
			OrderStatusDto orderStatusDto) {
		this.userService = userService;
		this.menuService = menuService;
		this.restaurantService = restaurantService;
		this.orderDto = orderDto;
		this.orderSourceDto = orderSourceDto;
		this.orderStatusDto = orderStatusDto;
	}
	
	/**
	 * Has side effects.
	 * 
	 * @param order
	 * @param authentication
	 * @return
	 */
	public Order validatePost(Order order, Authentication authentication) {
		Objects.requireNonNull(order.getPayment());
		Objects.requireNonNull(order.getDelivery());
		Objects.requireNonNull(order.getRestaurantId());
		Objects.requireNonNull(order.getContactPhone());
		Objects.requireNonNull(order.getOrderItems());
		ViewRestaurant restaurant = restaurantService.getRestaurantById(order.getRestaurantId());
		boolean isRestaurantOwner = userService.isRestaurantOwner(authentication, order, restaurant);
		if(!isRestaurantOwner) {
			order.setOrderStatus(getOrderStatus(OrderStatuses.NEW));
			order.setOrderSource(getOrderSource(OrderSources.E_SHOP));
			order.setCustomer(getCustomer(authentication));
		}
		order.setTimestamp(generateTimestamp());
		order.setOrderNumber(generateOrderNumber(order));
		order.setOrderItems(validateOrderItems(order));
		order.setDelivery(validateDelivery(order, restaurant));
		
		
		return order;
	}

	private Delivery validateDelivery(Order order, ViewRestaurant restaurant) {
		order.getDelivery().getDeliveryType();
		// TODO Auto-generated method stub
		return order.getDelivery();
	}

	private Collection<OrderItem> validateOrderItems(Order order) {
		order.getOrderItems().forEach(x -> x.setOrder(order));
		return order.getOrderItems();
//		List<OrderItem> existing = new ArrayList<>();
//		var menu = menuService.getMenuByRestaurant(order.getRestaurantId());
//		List<ViewItem> menuItems = menu.getCategories().stream()
//				.flatMap(menuItem -> menuItem.getItems().stream())
//				.collect(Collectors.toList());
//		
//		order.getOrderItems().forEach(orderItem -> {
//			var orderItemId = orderItem.getItemId();
//			menuItems.stream()
//					.map(menuItem -> menuItem.getId())
//					.filter(orderItemId::equals)
//					.findAny()
//					.ifPresent(x -> existing.add(orderItem));
//		});
//		existing.forEach(x -> x.setOrder(order));
//		return existing;
	}

	private UUID getCustomer(Authentication authentication) {
		if(authentication == null || authentication.getName() == null) {
			return null;
		}
		return UUID.fromString(authentication.getName());
	}

	private OrderSource getOrderSource(String orderSourceName) {
		return orderSourceDto.findOneByName(orderSourceName).orElseThrow();
	}
	private OrderStatus getOrderStatus(String orderStatusName) {
		return orderStatusDto.findOneByName(orderStatusName).orElseThrow();
	}

	private Instant generateTimestamp() {
		return Instant.now();
	}

	private Long generateOrderNumber(Order order) {
		var lastOrderNumber = orderDto
				.findFirstByRestaurantIdOrderByOrderNumberDesc(order.getRestaurantId())
				.map(Order::getOrderNumber)
				.orElse(0l);
		return lastOrderNumber + 1;
	}
}
