DROP TABLE IF EXISTS "address", "country", "delivery", "delivery_type", "order_table", "order_source", "order_status", "payment", "payment_type", "order_item" CASCADE;

CREATE TABLE IF NOT EXISTS address (
    id uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    country_id uuid,
    city text,
    zip_code text,
    street text,
    building text
);

CREATE TABLE IF NOT EXISTS "country" (
	id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	name text
);

CREATE TABLE IF NOT EXISTS "delivery" (
	id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	delivery_from TIME without time zone,
	delivery_to TIME without time zone,
	delivery_date DATE,
	delivery_type_id UUID,
	fee DECIMAL
);

CREATE TABLE IF NOT EXISTS "delivery_type" (
	id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	name text
);

CREATE TABLE IF NOT EXISTS "order_table" (
	id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	restaurant_id UUID,
	order_number NUMERIC,
	"timestamp" TIMESTAMP,
	note TEXT,
	contact_phone TEXT,
	order_status_id UUID,
	address_id UUID,
	order_source_id UUID,
	payment_id UUID,
	delivery_id UUID,
	customer UUID
);

CREATE TABLE IF NOT EXISTS "order_source" (
	id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	name text
);

CREATE TABLE IF NOT EXISTS "order_status" (
	id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	name text
);

CREATE TABLE IF NOT EXISTS "payment" (
	id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	payment_type_id UUID
);

CREATE TABLE IF NOT EXISTS "payment_type" (
	id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	name text
);

CREATE TABLE IF NOT EXISTS "order_item" (
	id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	order_id UUID,
	item_id UUID,
	count NUMERIC,
	note TEXT,
	unit_price DECIMAL
);

-- address FK
ALTER TABLE "address" ADD FOREIGN KEY (country_id) REFERENCES country(id);

-- delivery FK
ALTER TABLE "delivery" ADD FOREIGN KEY (delivery_type_id) REFERENCES delivery_type(id);

-- order FK
ALTER TABLE "order_table" ADD FOREIGN KEY (order_status_id) REFERENCES order_status(id);
--ALTER TABLE "order_table" ADD FOREIGN KEY (currency_id) REFERENCES currency(id);
ALTER TABLE "order_table" ADD FOREIGN KEY (address_id) REFERENCES address(id);
ALTER TABLE "order_table" ADD FOREIGN KEY (order_source_id) REFERENCES order_source(id);
ALTER TABLE "order_table" ADD FOREIGN KEY (payment_id) REFERENCES payment(id);
ALTER TABLE "order_table" ADD FOREIGN KEY (delivery_id) REFERENCES delivery(id);

-- payment FK
ALTER TABLE "payment" ADD FOREIGN KEY (payment_type_id) REFERENCES payment_type(id);

-- order_item FK
ALTER TABLE "order_item" ADD FOREIGN KEY (order_id) REFERENCES order_table(id);