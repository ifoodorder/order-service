{{ with $ip_address := (env "NOMAD_HOST_IP_app") }}
{{ with secret "pki_int/issue/cert" "role_name=order-service" "common_name=order-service.service.consul" "ttl=24h" "alt_names=_order-service._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.private_key }}
{{ end }}{{ end }}