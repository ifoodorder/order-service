# nginx config
HTTPS_PORT = ${NOMAD_PORT_app}
CERT_PATH = /local/cert.pem
CERT_KEY_PATH = /secrets/key.pem

# java config
JAVA_PORT = 8052
{{- with $instances := service "keycloak" }}
    {{- with $keycloak := index $instances 0 }}
KEYCLOAK_URL = "https://keycloak.service.consul:{{ $keycloak.Port }}/auth"
    {{- end }}
{{- end }}
KEYCLOAK_RESOURCE = Restaurant
KEYCLOAK_SECRET = {{ with secret "secret/order/prod" }}{{ .Data.ORDER_KEYCLOAK_SECRET }}{{ end }}
JDBC_URL = "jdbc:postgresql://postgresql.service.consul:5432/order"
JDBC_USER = {{ with secret "/database/creds/order" }}{{ .Data.username }}
JDBC_PWD = {{ .Data.password }}{{ end }}
H2_CONSOLE = false
RABBITMQ_HOST = "rabbitmq.service.consul"
RABBITMQ_PORT = "5671"
RABBITMQ_VIRTUALHOST = ifoodorder
SQL_INIT = never
LOG_FILE = logs/order-service.log
LOG_LEVEL = INFO

{{- with $instances := service "restaurant-service" }}
    {{- with $restaurant := index $instances 0 }}
RESTAURANT_HOST = "https://restaurant-service.service.consul:{{ $restaurant.Port }}"
    {{- end }}
{{- end }}

{{- with $instances := service "menu-service" }}
    {{- with $menu := index $instances 0 }}
MENU_HOST = "https://restaurant-service.service.consul:{{ $menu.Port }}"
    {{- end }}
{{- end }}