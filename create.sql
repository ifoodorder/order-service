CREATE TABLE "address" ( id UUID DEFAULT RANDOM_UUID() NOT NULL PRIMARY KEY, country_id UUID, city NVARCHAR2(200), zip_code NVARCHAR2(200), street NVARCHAR2(200), building NVARCHAR2(200));
CREATE TABLE "country" ( id UUID DEFAULT RANDOM_UUID() NOT NULL PRIMARY KEY, name NVARCHAR2(200));
--CREATE TABLE "currency" ( id UUID DEFAULT RANDOM_UUID() NOT NULL PRIMARY KEY, name NVARCHAR2(200));
CREATE TABLE "delivery" ( id UUID DEFAULT RANDOM_UUID() NOT NULL PRIMARY KEY, delivery_from TIME, delivery_to TIME, delivery_date DATE, delivery_type_id UUID, fee NVARCHAR2(500));
CREATE TABLE "delivery_type" ( id UUID DEFAULT RANDOM_UUID() NOT NULL PRIMARY KEY, name NVARCHAR2(200));
CREATE TABLE "order_table" ( id UUID DEFAULT RANDOM_UUID() NOT NULL PRIMARY KEY, restaurant_id UUID, order_number INTEGER, "timestamp" TIMESTAMP, note NVARCHAR2(200), contact_phone NVARCHAR2(200), order_status_id UUID, address_id UUID, order_source_id UUID, payment_id UUID, delivery_id UUID, customer UUID); --, currency_id UUID
CREATE TABLE "order_source" ( id UUID DEFAULT RANDOM_UUID() NOT NULL PRIMARY KEY, name NVARCHAR(200)); -- item UUID, alergen UUID);
CREATE TABLE "order_status" ( id UUID DEFAULT RANDOM_UUID() NOT NULL PRIMARY KEY, name NVARCHAR2(200));
CREATE TABLE "payment" ( id UUID DEFAULT RANDOM_UUID() NOT NULL PRIMARY KEY, payment_type_id UUID);
CREATE TABLE "payment_type" ( id UUID DEFAULT RANDOM_UUID() NOT NULL PRIMARY KEY, name NVARCHAR2(200));
CREATE TABLE "order_item" ( id UUID DEFAULT RANDOM_UUID() NOT NULL PRIMARY KEY, order_id UUID, item_id UUID, count INTEGER, note NVARCHAR(200), unit_price NVARCHAR(200)); 

-- address FK
ALTER TABLE "address" ADD FOREIGN KEY (country_id) REFERENCES country(id);

-- delivery FK
ALTER TABLE "delivery" ADD FOREIGN KEY (delivery_type_id) REFERENCES delivery_type(id);

-- order FK
ALTER TABLE "order_table" ADD FOREIGN KEY (order_status_id) REFERENCES order_status(id);
--ALTER TABLE "order_table" ADD FOREIGN KEY (currency_id) REFERENCES currency(id);
ALTER TABLE "order_table" ADD FOREIGN KEY (address_id) REFERENCES address(id);
ALTER TABLE "order_table" ADD FOREIGN KEY (order_source_id) REFERENCES order_source(id);
ALTER TABLE "order_table" ADD FOREIGN KEY (payment_id) REFERENCES payment(id);
ALTER TABLE "order_table" ADD FOREIGN KEY (delivery_id) REFERENCES delivery(id);

-- payment FK
ALTER TABLE "payment" ADD FOREIGN KEY (payment_type_id) REFERENCES payment_type(id);

-- order_item FK
ALTER TABLE "order_item" ADD FOREIGN KEY (order_id) REFERENCES order_table(id);

-- DEFAULTS

-- country DEFAULTS
INSERT INTO "country" (id, name) VALUES ('5989af93-007f-4f39-b405-a0d9a0ee7f4e', 'CZECH REPUBLIC');
INSERT INTO "country" (id, name) VALUES ('60918ab9-7d5e-49f5-9dcb-05c44e01e663', 'SLOVAKIA');
INSERT INTO "country" (id, name) VALUES ('0ef5316d-ae05-4c04-a3b3-9546061c81c2', 'POLAND');
INSERT INTO "country" (id, name) VALUES ('d258b3bb-6356-4e68-9d7d-0e7ccce70682', 'GERMANY');
INSERT INTO "country" (id, name) VALUES ('29c717fa-1393-441c-baad-68eb40cfb26c', 'AUSTRIA');

-- currency DEFAULTS
--INSERT INTO "currency" (id, name) VALUES ('407f8bdd-a614-40fe-b7eb-cb779ee12d6f', 'CZK');
--INSERT INTO "currency" (id, name) VALUES ('2ff1c27e-374c-4d6d-a5f4-234c45c5bf32', 'EUR');
--INSERT INTO "currency" (id, name) VALUES ('16d617ee-25a6-4882-868f-dd4e9ba68703', 'USD');

-- delivery_type DEFAULTS
INSERT INTO "delivery_type" (id, name) VALUES ('0cd7c94e-63b2-4f04-b330-555031939e7a', 'RESTAURANT');
INSERT INTO "delivery_type" (id, name) VALUES ('204b958e-3ffb-4c70-9131-7bdec89225c9', 'DELIVERY');
INSERT INTO "delivery_type" (id, name) VALUES ('49832d96-100d-4a61-b6b9-352a829bfc4d', 'PICKUP_POINT');

-- order_source DEFAULTS
INSERT INTO "order_source" (id, name) VALUES ('0a8c3263-e9d0-46fd-9479-5787e5b82c50', 'E-SHOP');
INSERT INTO "order_source" (id, name) VALUES ('fa5628d7-c103-41ca-8e0b-0a0c3863621a', 'RESTAURANT');
INSERT INTO "order_source" (id, name) VALUES ('5cb613ec-a905-495b-8db3-0537635027c5', 'THIRD_PARTY');

-- order_status DEFAULTS
INSERT INTO "order_status" (id, name) VALUES ('903dc221-7145-4e07-a7d3-565eb4e8a5a3', 'NEW');
INSERT INTO "order_status" (id, name) VALUES ('b3c09320-2091-4dc9-ba11-7639470e46dc', 'PENDING');
INSERT INTO "order_status" (id, name) VALUES ('7fd7e9e9-0a85-45ba-b7db-62e99b98e1e5', 'FINISHED');
INSERT INTO "order_status" (id, name) VALUES ('28cd219a-cff6-45c6-9cd5-9e051cf5fede', 'WAITING_FOR_CUSTOMER');
INSERT INTO "order_status" (id, name) VALUES ('8c662fd7-7eb2-4691-a51e-0b8e3d88bbd7', 'CANCELED');

-- payment_type DEFAULTS
INSERT INTO "payment_type" (id, name) VALUES ('d824b21b-922b-4851-b0b5-291e70313346', 'CASH');
INSERT INTO "payment_type" (id, name) VALUES ('af9a21f4-54c6-46f6-b165-e97f127c421a', 'CARD_ONLINE');
INSERT INTO "payment_type" (id, name) VALUES ('30946e55-8e20-47b3-a768-d0182f1beafa', 'CARD_COURIER');
INSERT INTO "payment_type" (id, name) VALUES ('20acb1bf-148a-4f00-8d25-9d39f862157a', 'BANK_TRANSFER');
INSERT INTO "payment_type" (id, name) VALUES ('3ca36ba5-767f-4192-85ce-9185f1d5d322', 'MEAL_VOUCHER');
INSERT INTO "payment_type" (id, name) VALUES ('7c09fe9f-eb42-4ae7-a547-eca528657520', 'BITCOIN');

-- DEMO

INSERT INTO "address" ( id, country_id, city, zip_code, street, building) VALUES ('b030ebca-04dc-4127-b794-74934996baeb', '5989af93-007f-4f39-b405-a0d9a0ee7f4e', 'Praha', '123 45', 'Demo street', '6');
INSERT INTO "address" ( id, country_id, city, zip_code, street, building) VALUES ('71830719-79c6-4c1d-83c5-b10461b5e6f3', '5989af93-007f-4f39-b405-a0d9a0ee7f4e', 'Praha', '789 01', 'Test strasse', '2');
INSERT INTO "address" ( id, country_id, city, zip_code, street, building) VALUES ('86e38369-1c1a-4698-a077-a1adcf7a6144', '5989af93-007f-4f39-b405-a0d9a0ee7f4e', 'Praha', '345 67', 'Preveiw bay', '8');
INSERT INTO "address" ( id, country_id, city, zip_code, street, building) VALUES ('265679dd-a500-4d78-bc7c-aecf25d31deb', '60918ab9-7d5e-49f5-9dcb-05c44e01e663', 'Bratislava', '901 23', 'Testovaci ulica', '4');

INSERT INTO "delivery" ( id, delivery_from, delivery_to, delivery_date, delivery_type_id, fee) VALUES ('f507d1e3-c7f2-4dd7-89b4-dea4bb646110', '14:00:00', '14:30:00', '2020-07-23','0cd7c94e-63b2-4f04-b330-555031939e7a','0');
INSERT INTO "delivery" ( id, delivery_from, delivery_to, delivery_date, delivery_type_id, fee) VALUES ('e2978e57-6d19-4e09-8ebe-821c8a62f225', '13:00:00', '14:00:00', '2020-07-24','49832d96-100d-4a61-b6b9-352a829bfc4d','10');
INSERT INTO "delivery" ( id, delivery_from, delivery_to, delivery_date, delivery_type_id, fee) VALUES ('25cc552e-3c5a-44f0-af59-368996296367', '18:00:00', '19:00:00', '2020-07-25','204b958e-3ffb-4c70-9131-7bdec89225c9','100');
INSERT INTO "delivery" ( id, delivery_from, delivery_to, delivery_date, delivery_type_id, fee) VALUES ('a2de4768-bcc9-4eb8-8971-65d73a05b68e', '15:00:00', '20:00:00', '2020-07-26','204b958e-3ffb-4c70-9131-7bdec89225c9','1000');

INSERT INTO "payment" ( id, payment_type_id) VALUES ('c41f67df-659c-41b3-9bf8-6862dc6bb858', 'd824b21b-922b-4851-b0b5-291e70313346');
INSERT INTO "payment" ( id, payment_type_id) VALUES ('f9b13e45-34a9-40c4-a406-fc82529ed1af', 'af9a21f4-54c6-46f6-b165-e97f127c421a');
INSERT INTO "payment" ( id, payment_type_id) VALUES ('54d38f29-5c6a-4fec-b10d-ceb8180236f8', '30946e55-8e20-47b3-a768-d0182f1beafa');
INSERT INTO "payment" ( id, payment_type_id) VALUES ('950789aa-756a-4ba6-b1dd-55b126c5aab4', '7c09fe9f-eb42-4ae7-a547-eca528657520');

INSERT INTO "order_table" ( id, restaurant_id, order_number, "timestamp", note, contact_phone, order_status_id, address_id, order_source_id, payment_id, delivery_id, customer) VALUES ('73543438-fc20-4317-9e1a-b6bd49172a88', '2986caca-8abe-4673-981c-46a33be4ba59', 1, '2005-12-31 23:59:59', 'No ice please.', '+420 123 456 789' ,'903dc221-7145-4e07-a7d3-565eb4e8a5a3', 'b030ebca-04dc-4127-b794-74934996baeb', 'fa5628d7-c103-41ca-8e0b-0a0c3863621a', 'c41f67df-659c-41b3-9bf8-6862dc6bb858', 'f507d1e3-c7f2-4dd7-89b4-dea4bb646110', NULL);
INSERT INTO "order_table" ( id, restaurant_id, order_number, "timestamp", note, contact_phone, order_status_id, address_id, order_source_id, payment_id, delivery_id, customer) VALUES ('100dd59f-f896-481b-ad05-9a5b972a4fd0', '6a7e1070-4746-49fb-80b3-5022b4488fd3', 2, '2005-12-31 23:59:59', 'Without a straw.', '+420 012 345 678', 'b3c09320-2091-4dc9-ba11-7639470e46dc', '71830719-79c6-4c1d-83c5-b10461b5e6f3', '0a8c3263-e9d0-46fd-9479-5787e5b82c50', 'f9b13e45-34a9-40c4-a406-fc82529ed1af', 'e2978e57-6d19-4e09-8ebe-821c8a62f225', NULL);
INSERT INTO "order_table" ( id, restaurant_id, order_number, "timestamp", note, contact_phone, order_status_id, address_id, order_source_id, payment_id, delivery_id, customer) VALUES ('3818e697-c473-4906-ab7d-a3f073fd021a', '1594f20b-536d-4260-bb59-19ae55b96b3b', 3, '2005-12-31 23:59:59', 'Without chilli, thanks!', '+420 901 234 567', '28cd219a-cff6-45c6-9cd5-9e051cf5fede', '86e38369-1c1a-4698-a077-a1adcf7a6144', '0a8c3263-e9d0-46fd-9479-5787e5b82c50', '54d38f29-5c6a-4fec-b10d-ceb8180236f8', '25cc552e-3c5a-44f0-af59-368996296367', NULL);
INSERT INTO "order_table" ( id, restaurant_id, order_number, "timestamp", note, contact_phone, order_status_id, address_id, order_source_id, payment_id, delivery_id, customer) VALUES ('ecacb32e-0f1d-46cf-a5c7-14f6f15a1f7e', '2986caca-8abe-4673-981c-46a33be4ba59', 4, '2005-12-31 23:59:59', 'Can I have the pizza boneless?', '+421 890 123 456', '8c662fd7-7eb2-4691-a51e-0b8e3d88bbd7', '265679dd-a500-4d78-bc7c-aecf25d31deb', '0a8c3263-e9d0-46fd-9479-5787e5b82c50', '950789aa-756a-4ba6-b1dd-55b126c5aab4', 'a2de4768-bcc9-4eb8-8971-65d73a05b68e', NULL);

INSERT INTO "order_item" ( id, order_id, item_id, count, note, unit_price) VALUES ('f311a3b6-3a71-46f7-bd50-76df9b1f1f2c', '73543438-fc20-4317-9e1a-b6bd49172a88', '566ec6e5-0461-41fc-bdde-b9e388c8570f', 1, 'Boneless please!', '50');
INSERT INTO "order_item" ( id, order_id, item_id, count, note, unit_price) VALUES ('5321ff4c-c043-4af1-9757-8a4b700f0c78', '100dd59f-f896-481b-ad05-9a5b972a4fd0', 'b35934aa-bc4e-4236-a61d-24f9da7c3ca9', 2, NULL, '100');
INSERT INTO "order_item" ( id, order_id, item_id, count, note, unit_price) VALUES ('5c77426d-c92c-4f53-abca-a2ccbb52338d', '3818e697-c473-4906-ab7d-a3f073fd021a', '93288581-1beb-4ea5-b3ea-03dea15f7f03', 3, NULL, '120');
INSERT INTO "order_item" ( id, order_id, item_id, count, note, unit_price) VALUES ('cfcb361c-251b-46d4-a635-c17be7aea3f5', 'ecacb32e-0f1d-46cf-a5c7-14f6f15a1f7e', '077f146d-6a74-484d-9b09-835541d677bb', 4, NULL, '500');